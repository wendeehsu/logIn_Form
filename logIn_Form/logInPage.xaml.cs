﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace logIn_Form
{
    public partial class logInPage : ContentPage
    {
        public logInPage()
        {
            InitializeComponent();
            GetUserInfo((string) Application.Current.Properties["userToken"]);
        }

        public class Img
        {
            public int id { get; set; }
            public string url { get; set; }
        }

        async private void GetUserInfo(string t)
        {
            //userImg.Source = "https://spartanfitness360.com/wp-content/uploads/2016/10/How-to-Snack-Blog-Post-Pic.jpg";
            try
            {
                //use token to request user info
                var client = new HttpClient();
                string url = "http://sample.dentall.io/api/photos/";
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", t);
                var response = await client.GetAsync(url);
                //Debug.WriteLine(response);
                int status = (int)response.StatusCode;
                if (status == 200)
                {
                    var result = await response.Content.ReadAsStringAsync();
                    Img[] imgs = JsonConvert.DeserializeObject<Img[]>(result);

                    Debug.WriteLine(result);
                    Carousel(imgs);
                }
            }
            catch (Exception ex)
            {
                await DisplayAlert("Oops", ex.Message, "Got it");
            }
        }

        async public void Carousel(Img[] pictures)
        {
            if (pictures.Length > 1)
            {
                while (true)
                {
                    foreach (Img i in pictures)
                    {
                        userImg.Source = i.url;
                        await Task.Delay(1500);
                    }
                }
            }
            else if (pictures.Length == 1)
            {
                userImg.Source = pictures[0].url;
            }
            else
            {
                userImg.IsVisible = false;
                text.IsVisible = true;
                text.Text = "Oops, you don't have pictures.";
                text.TextColor = Color.AliceBlue;

            }
        }



        async void LogOut_Clicked(object sender, System.EventArgs e)
        {
            Application.Current.Properties["userToken"] = "";
            await Navigation.PopAsync();
        }

        async void ChangePwd_Clicked(object sender, System.EventArgs e)
        {
            await Navigation.PushModalAsync(new ChangePwdPage());

        }
    }
}
