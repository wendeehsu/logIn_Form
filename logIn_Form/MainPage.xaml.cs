﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Foundation;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace logIn_Form
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            activityIndicator.IsVisible = false;
            activityIndicator.IsRunning = false;
            Application.Current.Properties["rememberMe"] = false;

        }

        public class User
        {
            public string password { get; set; }
            public string username { get; set; }
            public bool rememberMe { get; set; }
        }


        async private void LogIn_Clicked(object sender, EventArgs e){
            User newUser = new User();
            newUser.password = password.Text.ToString();
            newUser.username = account.Text.ToString();
            newUser.rememberMe = (bool) Application.Current.Properties["rememberMe"];

            string json = JsonConvert.SerializeObject(newUser);

            activityIndicator.IsVisible = true;
            activityIndicator.IsRunning = true;
            var logInSuccess = await PostFuncAsync(json);
            activityIndicator.IsVisible = false;
            activityIndicator.IsRunning = false;

            //var logInSuccess = await PostFuncAsync(json);

            if(logInSuccess){
                await Navigation.PushAsync(new logInPage());
            }else{
                await DisplayAlert("Oops", "There is problem with your account!", "Got it");
                
            }
        }

        async public Task<bool> PostFuncAsync(string json)
        {
            try
            {
                var client = new HttpClient();
                client.BaseAddress = new Uri("http://sample.dentall.io");

                var content = new StringContent(json, Encoding.UTF8, "application/json");
                HttpResponseMessage response = null;
                response = await client.PostAsync("/api/authenticate", content);

                // this result string should be something like: "{"token":"rgh2ghgdsfds"}"
                var result = await response.Content.ReadAsStringAsync();
                Debug.WriteLine(result);

                int status = (int)response.StatusCode;
                if (status == 200)
                {
                    var definition = new { id_token = "" };
                    var id1 = JsonConvert.DeserializeAnonymousType(result, definition);

                    string token = Convert.ToString(id1.id_token);
                    Application.Current.Properties["userToken"] = token;
                }
                return (status == 200);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return (false);
            }
        }

        void Handle_Toggled(object sender, Xamarin.Forms.ToggledEventArgs e)
        {
            bool rememberMe = e.Value;
            if(rememberMe){
                RememberMeText.TextColor = Color.Pink;
            }else{
                RememberMeText.TextColor = Color.Gray;
            }
            Application.Current.Properties["rememberMe"] = rememberMe;
        }
    }
}
