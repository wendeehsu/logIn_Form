﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace logIn_Form
{
    public partial class ChangePwdPage : ContentPage
    {
        public ChangePwdPage()
        {
            InitializeComponent();
        }

        public class ModifyPwd
        {
            public string currentPassword { get; set; }
            public string newPassword { get; set; }
            public ModifyPwd(string oldP, string newP)
            {
                currentPassword = oldP;
                newPassword = newP;
            }
        }


        async void Confirm_Clicked(object sender, System.EventArgs e)
        {
            if(newPwd.Text == confirmNewPwd.Text){
                ModifyPwd modifyPwd = new ModifyPwd(currentPwd.Text, newPwd.Text);
                string json = JsonConvert.SerializeObject(modifyPwd);

                var PostSuccess = await PostPassword(json);
                ShowMsg(PostSuccess);

            }else{
                await DisplayAlert("Oops", "new password is different from confirm new password", "Got it");
            }
        }

        async public Task<bool> PostPassword(string json)
        {
            try
            {
                string t = (string) Application.Current.Properties["userToken"];

                var client = new HttpClient();
                string url = "http://sample.dentall.io/api/account/change-password/";
                var content = new StringContent(json, Encoding.UTF8, "application/json");

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", t);
                var response = await client.PostAsync(url, content);

                Debug.WriteLine(response);
                int status = (int)response.StatusCode;

                return (status == 200);
            }
            catch (Exception ex)
            {
                await DisplayAlert("Oops", ex.Message, "OK");
                return (false);
            }
        }

        async private void ShowMsg(bool success)
        {
            string title;
            string message;
            if (success)
            {
                title = "Success";
                message = "You have changed your password!";
            }
            else
            {
                title = "Sorry";
                message = "Fail to change your password ><";
            }
            //create alert
            await DisplayAlert(title, message, "Got it");
            await Navigation.PopModalAsync();
        }


        async void Cancel_Clicked(object sender, System.EventArgs e)
        {
            await Navigation.PopModalAsync();
        }
    }
}
